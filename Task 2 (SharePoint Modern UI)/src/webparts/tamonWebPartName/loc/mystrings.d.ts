declare interface ITamonWebPartNameWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'TamonWebPartNameWebPartStrings' {
  const strings: ITamonWebPartNameWebPartStrings;
  export = strings;
}
