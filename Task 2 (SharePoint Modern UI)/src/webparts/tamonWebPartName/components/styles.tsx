export const wrapperStyle: any = {
    background:'lightgrey',
    fontFamily: 'Georgia',
    textAlign: 'center',
  }

  export const headingStyle: any = {
      textTransform: 'capitalize',
      fontWeight: 'bold',
      textShadow: '1px 1px 2px red, 0 0 1em rgb(33, 33, 39), 0 0 0.2em rgb(25, 25, 105)',
      padding: '25px 0',
  }
  
  export const employeesStyle: any = {
      display: 'flex',
      justifyContent: 'space-evenly',
      flexWrap: 'wrap',
      padding: '1em',
  }
  
  export const employeeBoxStyle: any = {
      textAlign: 'center',
      display: 'flex',
      flexDirection: 'column',
      border: '1px solid grey',
      borderRadius: '10px',
      padding: '0 .5em .5em .5em',
      marginTop: '1em',
      backgroundColor: 'rgba(255, 255, 255, 0.842)',
      boxShadow: '10px 5px 5px rgb(99, 92, 196)',
  }
  
  export const accentedStyle: any = {
      fontWeight: 'bold',
      fontSize: '20px',
      color:'brown',
  }
  
  export const employeePhotoStyle: any = {
      height: '150px',
      width: '250px',
      display: 'block',
      borderRadius: '10px',
  }