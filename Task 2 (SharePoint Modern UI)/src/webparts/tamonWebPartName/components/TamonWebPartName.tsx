import * as React from 'react';
import styles from './TamonWebPartName.module.scss';
import { WebPartContext } from "@microsoft/sp-webpart-base";
import { escape } from '@microsoft/sp-lodash-subset';
import { sp } from "@pnp/sp";
import "@pnp/sp/webs";
import "@pnp/sp/lists";
import "@pnp/sp/items";

import { wrapperStyle, 
        headingStyle, 
        employeesStyle, 
        employeeBoxStyle, 
        accentedStyle, 
        employeePhotoStyle } from './styles'

export interface IProps {
  description: string;
  context: WebPartContext;
}

interface IState {
  employees: any[]
}

export default class TamonWebPartName extends React.Component<IProps, IState> {
  state={
    employees: null
  }

  public async componentDidMount(){
    sp.setup({
      spfxContext: this.props.context
    });

    const response: any[] = await sp.web.lists.getByTitle("mainList").items.get();
    this.setState({employees: response});
  }

  public async addPerson(){

      const item: any = await sp.web.lists.getByTitle("mainList").items.add({
      Title: 'Petr',
      LastName: 'Petrov',
      Billable: true,
      Sex: "Male",
      Roles: "Head of IT Support"
    });
  }  

  public render(): React.ReactElement<IProps> {
    return (
      <div style={wrapperStyle}>
          <h1 style={headingStyle}>Our employees</h1>
          {this.state.employees && 
            <div style={employeesStyle}>{this.state.employees.map(element=>{
            return (
              <article style={employeeBoxStyle}>
                <p>First name: <span style={accentedStyle}> {element.Title} </span></p>
                <p>Last name: <span style={accentedStyle}> {element.LastName} </span></p>
                <p style={{textDecoration:'underline'}}>Position: {element.Roles} </p>
                <img style={employeePhotoStyle} alt='employee-photo' src={element.Photo}></img>
              </article>
            )
          })}</div>
          }
            <p>Press the button below and our newcomer <span style={accentedStyle}>Petya</span> will be added to this list!</p>
            <button onClick={this.addPerson}>Click here + REFRESH the page :)</button>
      </div>
    );
  }
}
