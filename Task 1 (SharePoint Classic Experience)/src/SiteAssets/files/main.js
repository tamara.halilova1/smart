async function getData() {
  const query = `${_spPageContextInfo.webAbsoluteUrl}/_api/web/lists/getbytitle('People')/items`
  const request = await fetch(query, {
    headers: {
      accept: "application/json;odata=verbose"
    }
  })
  const result = await request.json();
  return result
}

getData().then(res=>{

  res.d.results.forEach(element=>{
    console.log(element);

    const container = document.createElement('div');
    container.classList.add('employee-box');
    container.innerHTML = `
      <p>First name: <span class='accented'> ${element.Title} </span></p>
      <p>Last name: <span class='accented'> ${element.Surname} </span></p>
      <p style='font-style: italic;text-decoration: underline;'>Position: ${element.Roles} </p>
      <img class='employee-photo' alt='employee-photo' src='${element.OData__x0076_xq7}'>
      
    `;
    document.querySelector('.employees').append(container);

  })
});
